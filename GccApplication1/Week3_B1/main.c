#define F_CPU 8000000UL
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#define LCD_E 	3
#define LCD_RS	2

void lcd_strobe_lcd_e(void);
void init_4bits_mode(void);
void display_text(char *str);
void lcd_write_data(unsigned char byte);
void lcd_write_command(unsigned char byte);
void set_cursor(int line, int position);

void lcd_strobe_lcd_e(void)
{
	PORTC |= (1<<LCD_E);	// E high
	_delay_ms(1);			// nodig
	PORTC &= ~(1<<LCD_E);  	// E low
	_delay_ms(1);			// nodig?
}

void init_4bits_mode(void)
{
	// PORTC output mode and all low (also E and RS pin)
	DDRC = 0xFF;
	PORTC = 0x00;

	// Step 2 (table 12)
	PORTC = 0x20;	// function set
	lcd_strobe_lcd_e();

	lcd_write_command(0x28); // function set

	lcd_write_command(0x0F); // Display on/off control

	lcd_write_command(0x06); // Entry mode set

}

void display_text(char *str)
{
	 while(*str) {
	 	lcd_write_data(*str++);
		_delay_ms(250);
	 }
}

void lcd_write_data(unsigned char byte)
{
	// First nibble.
	PORTC = byte;
	PORTC |= (1<<LCD_RS);
	lcd_strobe_lcd_e();

	// Second nibble
	PORTC = (byte<<4);
	PORTC |= (1<<LCD_RS);
	lcd_strobe_lcd_e();
}

void lcd_write_command(unsigned char byte)
{
	// First nibble.
	PORTC = byte;
	PORTC &= ~(1<<LCD_RS);
	lcd_strobe_lcd_e();

	// Second nibble
	PORTC = (byte<<4);
	PORTC &= ~(1<<LCD_RS);
	lcd_strobe_lcd_e();
}

void set_cursor(int line, int pos)  // max 16
{
	if((pos > 16 || pos < 0) && (line > 1 || line < 0)) {
		return;
	}

	int commandToWrite = 0x80 + (line * 0x40) + pos;
	lcd_write_command(commandToWrite);
}

int main( void )
{
	_delay_ms(1500);
	// Init I/O
	DDRD = 0xFF;			// PORTD(7) output, PORTD(6:0) input
	// Init LCD
	init_4bits_mode();

	// Write sample string
	set_cursor(1, 4);
	display_text("Hello");
	set_cursor(1, 10);
	display_text("OK");
	set_cursor(0, 3);
	display_text("Hoi");
	set_cursor(0, 15);
	display_text("012");
	
	// Loop forever
	while (1)
	{
		PORTD ^= (1<<7);	// Toggle PORTD.7
		_delay_ms( 250 );
	}
	return 1;
}
