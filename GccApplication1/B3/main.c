/*
 * B3
 *
 * Created: 9-2-2017 12:02:07
 * Author : Joey
 */ 

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

int index = 0;

/************************/
void wait( int ms )
/* 
short:			Busy wait number of millisecs
inputs:			int ms (Number of millisecs to busy wait)
outputs:	
notes:			Busy wait, not very accurate. Make sure (external)
				clock value is set. This is used by _delay_ms inside
				util/delay.h
Version :    	DMK, Initial code
***********************/
{
	for (int i=0; i<ms; i++)
	{
		_delay_ms( 1 );		// library function (max 30 ms at 8MHz)
	}
}

/************************/
ISR( INT1_vect )
{
	if(index != 9){
		index++;
	}
	else{
		index = 0;
	}
	wait(300);
}

/************************/
ISR( INT0_vect )
/***********************/
{
	if(index != 0){
		index--;
	}
	else{
		index = 10;
	}
	wait(300);
}


int main(void)
{
	const unsigned char numbers[10] = {0x3F, 0x06, 0x5B, 0x4F, 0x66, 0x6D, 0x7D, 0x07, 0x7F, 0x6F};

	// Init I/O
	DDRD = 0xF0;		// PORTD(7:4) output, PORTD(3:0) input
	DDRA = 0xFF;

	// Init Interrupt hardware
	EICRA |= 0x0B;			// INT1 falling edge, INT0 rising edge
	EIMSK |= 0x03;			// Enable INT1 & INT2
	
	// Enable global interrupt system
	//SREG = 0x80;			// Of direct via SREG of via wrapper
	sei();

	//PORTA = 0x01;

    /* Replace with your application code */
    while (1) 
    {
		PORTA = numbers[index];
		wait(500);
    }
}
