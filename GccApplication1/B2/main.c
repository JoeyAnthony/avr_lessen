/*
 * les-2.c
 *
 * Created: 9-2-2017 12:02:07
 * Author : nards
 */ 

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>


/************************/
void wait( int ms )
/* 
short:			Busy wait number of millisecs
inputs:			int ms (Number of millisecs to busy wait)
outputs:	
notes:			Busy wait, not very accurate. Make sure (external)
				clock value is set. This is used by _delay_ms inside
				util/delay.h
Version :    	DMK, Initial code
***********************/
{
	for (int i=0; i<ms; i++)
	{
		_delay_ms( 1 );		// library function (max 30 ms at 8MHz)
	}
}

/************************/
ISR( INT1_vect )
/* 
short:			ISR INT0
inputs:			
outputs:	
notes:			Set PORTD.5
Version :    	DMK, Initial code
***********************/
{
	if(PORTA != 0x80)
		PORTA <<=1;
	else
		PORTA = 0x01;
	wait(300);
}

/************************/
ISR( INT2_vect )
/* 
short:			ISR INT1
inputs:			
outputs:	
notes:			Clear PORTD.5
Version :    	DMK, Initial code
***********************/
{
	if(PORTA != 0x01)
		PORTA >>=1;
	else
		PORTA = 0x80;
	wait(300);
}

/************************/ 
int main( void )
/* 
short:			main() loop, entry point of executable
inputs:			
outputs:	
notes:			Slow background task after init ISR
Version :    	DMK, Initial code
***********************/
{
	// Init I/O
	DDRD = 0xF0;		// PORTD(7:4) output, PORTD(3:0) input	
	DDRA = 0xFF;

	// Init Interrupt hardware
	EICRA |= 0x0F;
	//EICRB |= 0x02;			// INT1 falling edge, INT0 rising edge
	EIMSK |= 0x06;			// Enable INT1 & INT2
	
	// Enable global interrupt system
	//SREG = 0x80;			// Of direct via SREG of via wrapper
	sei();				

	PORTA = 0x01;

	while (1)
	{						
	}

	return 1;
}