/*
 * Week4_B3.c
 *
 * Created: 9-3-2017 13:44:04
 * Author : Joey
 */ 

#include <avr/io.h>
#include <util/delay.h>

#define BIT(x)	(1 << (x))
#define LCD_E 	3
#define LCD_RS	2

void lcd_strobe_lcd_e(void)
{
	PORTC |= (1<<LCD_E);	// E high
	_delay_ms(1);			// nodig
	PORTC &= ~(1<<LCD_E);  	// E low
	_delay_ms(1);			// nodig?
}

// Initialize ADC:
void adcInit( void )
{
	ADMUX = 0b11100001;			// AREF=2,56 V, result left adjusted, channel1 at pin PF1
	ADCSRA = 0b10000110;		// ADC-enable, no interrupt, no free running, division by 64
}

void init_4bits_mode(void)
{
	// PORTC output mode and all low (also E and RS pin)
	DDRC = 0xFF;
	PORTC = 0x00;

	PORTC = 0x33;
	lcd_strobe_lcd_e();
	_delay_ms(4);
	PORTC = 0x33;
	lcd_strobe_lcd_e();
	_delay_ms(4);
	PORTC = 0x33;
	lcd_strobe_lcd_e();
	_delay_ms(4);

	// Step 2 (table 12)
	PORTC = 0x20;	// function set
	lcd_strobe_lcd_e();

	lcd_write_command(0x28); // function set

	lcd_write_command(0x0C); // Display on/off control

	lcd_write_command(0x06); // Entry mode set

}

void display_text(char *str)
{
	while(*str) {
		lcd_write_data(*str++);
		_delay_ms(250);
	}
}

void lcd_write_data(unsigned char byte)
{
	// First nibble.
	PORTC = byte;
	PORTC |= (1<<LCD_RS);
	lcd_strobe_lcd_e();

	// Second nibble
	PORTC = (byte<<4);
	PORTC |= (1<<LCD_RS);
	lcd_strobe_lcd_e();
}

void lcd_write_command(unsigned char byte)
{
	// First nibble.
	PORTC = byte;
	PORTC &= ~(1<<LCD_RS);
	lcd_strobe_lcd_e();

	// Second nibble
	PORTC = (byte<<4);
	PORTC &= ~(1<<LCD_RS);
	lcd_strobe_lcd_e();
}

// Main program: Counting on T1
int main( void )
{

const unsigned char numbers[10] = {0x3F, 0x06, 0x5B, 0x4F, 0x66, 0x6D, 0x7D, 0x07, 0x7F, 0x6F};

	display_text(100);
	DDRF = 0x00;					// set PORTF for input (ADC)
	DDRA = 0xFF;					// set PORTA for output
	adcInit();						// initialize ADC
	init_4bits_mode();
	char str[3];

	while (1)
	{
		ADCSRA |= BIT(6);				// Start ADC
		while ( ADCSRA & BIT(6) ) ;
		PORTA = ADCH;					// Show MSB (bit 9:2) of AD
		sprintf(str, "%i", ADCH);
		display_text(str);
		lcd_write_command(0x80);
		_delay_ms(10000);					// every 50 ms (busy waiting)
	}
}