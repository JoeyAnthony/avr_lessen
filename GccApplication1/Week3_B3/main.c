#define F_CPU 8000000UL
#include <avr/io.h>
#include <util/delay.h>	

ISR( TIMER2_COMP_vect )
{
	int msCount = 0;
	int seconden;
	int minuten;
	int uren;
	msCount++; // Increment ms counter
	if ( msCount == 1000 )
	{
		PORTC ^= 0x00; // Toggle bit 0 van PORTC
		msCount = 0; // Reset ms_count value
		seconden++;
		if ( seconden ==60 )
		{
			seconden = 0;
			minuten++;
			if (minuten == 60 )
			{
				minuten = 0;
				uren++;
			}
		}
	}
}


int main(void)
{
    TCCR2 = 0b00011101;
	DDRC = 0xFF;
	TIMSK |= 0x07;
	OCR2 = 250;

    while(1)
    {
		if ( TCNT1 >= 15624)
		{
			PORTC ^= (1 << 0) ; // Toggle the LED
			TCNT1 = 0; // Reset timer value
		}
    }
}

