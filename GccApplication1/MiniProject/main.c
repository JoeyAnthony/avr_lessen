/*
 * MiniProject.c
 *
 * Created: 23-3-2017 09:17:32
 * Author : Joey
 */ 
#define F_CPU 8000000
#include <util/delay.h>
#include <stdio.h>
#include "lcd.h"

void initX(void)
{
	ADMUX = 0b01100001;				// AREF=VCC, result left adjusted, channel1 at pin PF0
	ADCSRA = 0b11100110;			// ADC-enable, no interrupt, start, free running, division by 64
}

void initY(void)
{
	ADMUX = 0b01100000;				// AREF=VCC, result left adjusted, channel1 at pin PF1
	ADCSRA = 0b11100110;			// ADC-enable, no interrupt, start, free running, division by 64
}

int main(void)
{
	DDRF = 0x00;					// set PORTF for input (ADC)
	DDRG = 0xFF;					// PORTG for output to read the touch module
	DDRC = 0xFF;

	_delay_ms(10);
	init_4bits_mode();				//init for display
	_delay_ms(10);
	SetCursor(0,5);
	display_text("X: ");
	SetCursor(1,5);
	display_text("Y: ");

	while (1)
	{
		char hoi[25];

		initX();					//initialize x value
		PORTG = 0x10;
		unsigned int valueX = ADCH;

		_delay_ms(1);

		sprintf(hoi, "%u ", valueX);
		SetCursor(0,8);
		display_text(hoi);			//display x value

		_delay_ms(1);

		initY();					//initialize y value
		PORTG = 0x08;
		unsigned int valueY = ADCH;

		_delay_ms(1);

		sprintf(hoi, "%u ", valueY);
		SetCursor(1, 8);
		display_text(hoi);			//display y value

		_delay_ms(300);			//wait for restart
	}
}

