/*
 * lcd.c
 *
 * Created: 23-3-2017 12:00:08
 *  Author: Joey
 */
 #define F_CPU 8000000
 #include "lcd.h"


void lcd_strobe_lcd_e(void)
{
	PORTC |= (1<<LCD_E);	// E high
	_delay_ms(1);			// nodig
	PORTC &= ~(1<<LCD_E);  	// E low
	_delay_ms(1);			// nodig?
}

void init_4bits_mode(void)
{
	// PORTC output mode and all low (also E and RS pin)
	DDRC = 0xFF;
	PORTC = 0x00;

	PORTC = 0x33;
	lcd_strobe_lcd_e();
	_delay_ms(1);
	PORTC = 0x33;
	lcd_strobe_lcd_e();
	_delay_ms(1);
	PORTC = 0x33;
	lcd_strobe_lcd_e();
	_delay_ms(1);

	// Step 2 (table 12)
	PORTC = 0x20;	// function set
	lcd_strobe_lcd_e();

	lcd_write_command(0x28); // function set

	lcd_write_command(0x0C); // Display on/off control

	lcd_write_command(0x06); // Entry mode set

	lcd_write_command(0x01);

	lcd_write_command(0x40);
}

void display_text(char *str)
{
	while(*str) {
		lcd_write_data(*str++);
		_delay_ms(25);
	}
}

void lcd_write_data(unsigned char byte)
{
	// First nibble.
	PORTC = byte;
	PORTC |= (1<<LCD_RS);
	lcd_strobe_lcd_e();

	// Second nibble
	PORTC = (byte<<4);
	PORTC |= (1<<LCD_RS);
	lcd_strobe_lcd_e();
}

void lcd_write_command(unsigned char byte)
{
	// First nibble.
	PORTC = byte;
	PORTC &= ~(1<<LCD_RS);
	lcd_strobe_lcd_e();

	// Second nibble
	PORTC = (byte<<4);
	PORTC &= ~(1<<LCD_RS);
	lcd_strobe_lcd_e();
}

void SetCursor(int line, int pos)  // max 16
{
	if((pos > 16 || pos < 0) && (line > 1 || line < 0)) {
		return;
	}

	int commandToWrite = 0x80 + (line * 0x40) + pos;
	lcd_write_command(commandToWrite);
}
