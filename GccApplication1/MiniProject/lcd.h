/*
 * lcd.h
 *
 * Created: 23-3-2017 11:59:54
 *  Author: Joey
 */ 


#ifndef LCD_H_
#define LCD_H_

#include <avr/io.h>
#include <util/delay.h>

#define BIT(x)	(1 << (x))
#define LCD_E 	3
#define LCD_RS	2

void lcd_strobe_lcd_e(void);
void init_4bits_mode(void);
void display_text(char *str);
void lcd_write_data(unsigned char byte);
void lcd_write_command(unsigned char byte);
void SetCursor(int, int);

#endif /* LCD_H_ */