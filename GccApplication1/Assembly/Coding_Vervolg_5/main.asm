; demoprogramma 6_B: 16 bits optelling
;
.INCLUDE "m128def.inc"
;
.CSEG
.ORG 	$0000
		rjmp main
;
.CSEG
.ORG	$0200
;
main:				

	LDI YH, $04 ; Y <- $0400 adres in data memory
	LDI YL, $00 ;
	; LDI YH, HIGH(a) ;
	; LDI YL, LOW(a) ;
	LDD	R8, Y+0	; Haal waarden op uit data memory
	LDD	R9, Y+!
	LDD	R10, Y+2
	LDD R11, Y+3
	LDD R12, Y+4 
	LDD R13, Y+5 
	LDD R14, Y+6 
	LDD R15, Y+7 

	mov	r4, r8		
	add	r4, r12		
					
	mov	r5, r9		 
	adc	r5, r13
	
	mov r6, r10
	adc r6, r14
	
	mov r7, r11
	adc r7, r15		

	STD Y+8,R4 ; Sla resultaat op in data memory
	STD Y+9,R5 
	STD Y+10,R6
	STD Y+11,R7

no_end:   			; unlimited loop, when done
		rjmp no_end	
