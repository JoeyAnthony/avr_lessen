;
; Coding_Vervolg_6.asm
;
; Created: 6-4-2017 17:27:31
; Author : Joey & Nard
;


; Replace with your application code
start:
	ldi R17, 7

loop1:
	ldi R16, 0b0000001
	sts PORTA, R16
	lsl R16
	dec R17
	brne loop1

reset:
	ldi R17, 7

loop2:	
	ldi R16, 0b1000000
	sts PORTA, R16
	lsr R16
	dec R17
	brne loop2

end:
    rjmp end
