#define F_CPU 8000000UL
#include<avr/io.h>
#include<avr/interrupt.h>
#include <util/delay.h>
#include <string.h>
#include <stdio.h>

#define LCD_E 	3
#define LCD_RS	2

void lcd_strobe_lcd_e(void);
void init_4bits_mode(void);
void display_text(char *str);
void lcd_write_data(unsigned char byte);
void lcd_write_command(unsigned char byte);

void lcd_strobe_lcd_e(void)
{
	PORTC |= (1<<LCD_E);	// E high
	_delay_ms(1);			// nodig
	PORTC &= ~(1<<LCD_E);  	// E low
	_delay_ms(1);			// nodig?
}

void init_4bits_mode(void)
{
	// PORTC output mode and all low (also E and RS pin)
	DDRC = 0xFF;
	PORTC = 0x00;

	PORTC = 0x20;	// function set
	lcd_strobe_lcd_e();

	lcd_write_command(0x28); // function set

	lcd_write_command(0x0C); // Display on/off control

	lcd_write_command(0x06); // Entry mode set
}

void display_text(char *str)
{
	while(*str) {
		lcd_write_data(*str++);
	}
}

void lcd_write_data(unsigned char byte)
{
	// First nibble.
	PORTC = byte;
	PORTC |= (1<<LCD_RS);
	lcd_strobe_lcd_e();

	// Second nibble
	PORTC = (byte<<4);
	PORTC |= (1<<LCD_RS);
	lcd_strobe_lcd_e();
}

void lcd_write_command(unsigned char byte)
{
	// First nibble.
	PORTC = byte;
	PORTC &= ~(1<<LCD_RS);
	lcd_strobe_lcd_e();

	// Second nibble
	PORTC = (byte<<4);
	PORTC &= ~(1<<LCD_RS);
	lcd_strobe_lcd_e();
}

void ClearDisplay()
{
	lcd_write_command(0x01);
}

int main()
{
	_delay_ms(1500);
	init_4bits_mode();
	DDRD = 0x00;
	TCCR2 = 0x07;
	
	int time;
	char timer[5];

	while(1)
	{
		ClearDisplay();
		time = TCNT2;
		sprintf(timer, "%d", time);
		display_text(timer);
		_delay_ms(500);
	}
}